var webpack = require('webpack');
module.exports = {
  entry: {
    app:'./src/app.js',
    vendor:[
      'angular',
      'angular-ui-router',
      'angular-ui-bootstrap',
      'jquery'
    ]
  },
  output: {
    path: 'dist',
    filename: '[name].bundle.js',
    publicPath: "dist"
    // temperorily keeps the bundle.js file in the webpack-dev-server, this path is not visible outside, but serves packed bundle.js from here
  },
  plugins: [
      new webpack.DefinePlugin({
        'process.env': {
          'NODE_ENV': JSON.stringify('production')
        }
      }),
      new webpack.optimize.CommonsChunkPlugin("vendor", "vendor.bundle.js"),
      new webpack.optimize.UglifyJsPlugin({minimize: true, sourceMap: true})
  ],
  devtool:'cheap-module-source-map',
  module: {
    loaders: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        loader: 'babel'
      },
      {
        test: /\.css$/,
        loader: 'style-loader!css-loader'
      },
      {
        test: /\.html/,
        loader: 'html-loader'
      }
    ]
  }
};
