import dashboardTemplate from './dashboard.template.html';
import dashboardController from './dashboard.controller';

angular.module('dashboard').component('dashboard', {
    template: dashboardTemplate,
    controller:dashboardController,
    controllerAs:'dashboardCtrl'
})