errorDialogController.$inject = ['$uibModalInstance', 'params'];

export default function errorDialogController($uibModalInstance, params) {
    let self = this;
    
    self.text = params.errorMessage;
    if (params.stackTrace) {
        self.text = self.text + '\n' + params.stackTrace;
    }
    self.errorMessage = params.errorMessage;
    self.stackTrace = params.stackTrace;
    
    self.ok = function () {
        $uibModalInstance.close({});
    };

    self.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
}