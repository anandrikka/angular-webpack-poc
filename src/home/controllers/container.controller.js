import errorDialogTemplate from '../templates/error-dialog.template.html';

const containerCtrl = function($scope, UserService, $rootScope, config, $uibModal, $http, common, $log) {
    let self = this;
    let events = config.events;
    
    self.isBusy = false;
    self.showError = false;

    function activate() {
        let requests = [];
        for(var i=0; i<100; i++) {
            requests.push($http.get('https://jsonplaceholder.typicode.com'));
        }
        common.activateParentController([requests], 'ErrorDialogController').then((data) => {

        }, (error) => {

        })
    }

    //activate();
    
    self.spinnerOptions = {
        radius: 40,
        lines: 7,
        length: 0,
        width: 30,
        speed: 1.7,
        corners: 1.0,
        trail: 100,
        color: '#5a91cc'
    };

    function toggleSpinner(on) {
        if(!self.showError) {
            self.isBusy = on;
        }
    }

    function captureInner(error) {
        var errorString = 'Error: ' + error.class + ' ' + error.data + ' ' + error.error + ' ' + error.location + '\n';
        if (error.stack) {
            errorString = errorString + 'Call Stack:\n';
            for (var i = error.stack.length - 1; i >= 0; --i) {
                var s = error.stack[i];
                errorString = errorString + '' + i + ':' + s.error + ', place:' + s.place + ',mcode:' + s.mcode + '\n';
            }
        }
        if (error.inner) {
            errorString = errorString + captureInner(error.inner);
        }
        return errorString;
    }

    function showError(data) {
        self.showError = true;
        self.isBusy = false;
        let errorMessage = '';
        if (data.exception) {
            let exc = data.exception;
            if (exc.config) {
                if (exc.status) {
                    errorMessage = errorMessage + 'Command:' + exc.status + ' ' + exc.statusText + ' ' + exc.config.url + '\n';
                    if (exc.status === 500) {
                        if (exc.data && exc.data.class) {
                            let error = exc.data; // cache error
                            errorMessage = errorMessage + captureInner(error);
                        }
                    }
                }
            }
        }
        else if (data.data){
            errorMessage = angular.toJson(data.data);
        } else {
            errorMessage = angular.toJson(data);
        }
        if (!errorMessage && (data.exception && (typeof data.exception === 'string'))) {
            errorMessage = data.exception;
        }

        let stackTrace = data && data.exception ? data.exception.stack : '';
        let modalInstance = $uibModal.open({
            template: errorDialogTemplate,
            controller: 'ErrorDialogController',
            controllerAs:'errorDialogCtrl',
            resolve: {
                params: () => {
                    return { errorMessage: errorMessage, stackTrace: stackTrace };
                }
            }
        });
        modalInstance.result.then((value) => {
            self.showError = false;
        }, () => {
            self.showError = false;
        });
    }

    $rootScope.$on('$stateChangeStart', (event, toState, toParams, fromState, fromParams) => {
        $log.log('Route Change Event Triggered from: ', fromState.url,  'to',  toState.url);
        toggleSpinner(true); 
    });

    $rootScope.$on(events.showErrorDialog, (event, data) => {
        showError(data);
    });

    $rootScope.$on(events.spinnerToggle, (event, data) => {
        toggleSpinner(data.show);
    });

    $rootScope.$on(events.activateController, (event, data) => {
            $log.log('Activate Controller Event Recieved for: ', data.controllerId);
            toggleSpinner(false);
        }
    );

    $rootScope.$on(events.activateControllerFailed, (event, data) => {
            toggleSpinner(false);
            showError(data);
        }
    );
};

containerCtrl.$inject = ['$scope', 'UserService', '$rootScope', 'config', '$uibModal', '$http', 'common', '$log'];

export default containerCtrl;
