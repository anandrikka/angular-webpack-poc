import errorDialogController from './error-dialog.controller';

angular.module('sep').controller('ErrorDialogController', errorDialogController);