'use strict';

import navBarController from '../controllers/navbar.controller';
import navbarTemplate from '../templates/navbar.template.html';

angular.module('sep').component('navbar', {
	template: navbarTemplate,
	controllerAs: 'navbarCtrl',
	controller: navBarController
})