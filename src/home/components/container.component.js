'use strict';

import containerCtrl from '../controllers/container.controller';

angular.module('sep').component('appContainer', {
    template: require('../templates/container.template.html'),
    controllerAs:'containerCtrl',
    controller: containerCtrl
})