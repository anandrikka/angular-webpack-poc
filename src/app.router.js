routing.$inject = ['$stateProvider', '$urlRouterProvider'];

export default function routing($stateProvider, $urlRouterProvider) {
	
	$urlRouterProvider.otherwise('/dashboard');
	
	$stateProvider
		.state('dashboard', {
			url:'/dashboard',
			template: '<dashboard></dashboard>'
		})
		.state('qualification', {
			url:'/qualification',
			template: '<div>Qualification</div>'
		})
		.state('training', {
			url:'/training',
			template: '<div>Training</div>'
		})
		.state('requests', {
			url:'/requests',
			template: '<div>Requests</div>'
		})
		.state('faq', {
			url:'/faq',
			template: '<div>FAQ\'s</div>'
		})
		.state('documentation', {
			url:'/documentation',
			template: '<div>Documentation</div>'
		})
		.state('admin', {
			url:'/admin',
			template: '<div>Admin</div>'
		})
		.state('links', {
			url:'/links',
			template: '<div>Links</div>'
		})
}