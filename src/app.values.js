'use strict';

let events = {
    showErrorDialog: 'error.dialog',
    spinnerToggle: 'spinner.toggle',
    activateParentController: 'activate.parent.controller',
    activateController: 'activate.controller',
    activateControllerFailed: 'activate.controller.failed'
}
let config = {
    events: events
}

export default config;