decorators.$inject = ['$provide']

export default function decorators($provide) {
    $provide.decorator('$exceptionHandler', ['$injector', '$delegate', ($injector, $delegate) => {
        return (exception, cause) => {
            $delegate(exception, cause);
            let errorData = {exception: exception, cause: cause};
            let common = $injector.get('common');
            common.broadcastErrorDialogEvent(errorData);
        }
    }]);
    $provide.decorator('$q', ['$delegate', ($delegate) => {
        function seqAll(promises) {
            var deferred = $delegate.defer();
            var results = [];
            var j = 0;
            recursive(promises[j]);
            function recursive(promise) {
                j++;
                promise.then((data) => {
                    results.push(data);
                    if (j < promises.length) {
                        recursive(promises[j]);
                    } else {
                        deferred.resolve(results);
                    }
                }, (error) => {
                    //todo: Error data need to be made generic
                    deferred.reject('promises[' + (j - 1) + ']' + ' rejected with status: ' + error.status);
                    return;
                });
            }
            return deferred.promise;
        }
        $delegate.seqAll = seqAll;
        return $delegate;
    }]);
}