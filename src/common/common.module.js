let commonModule = angular.module('common', [])

commonModule.provider('commonConfig', [function() {
    this.config = {};
    this.$get = function() {
      return {
          config: this.config
      }
    };
}])

require('./services');
require('./controllers');
require('./components');

export default commonModule;

