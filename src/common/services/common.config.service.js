configService.$inject = ['$rootScope', '$q', '$window', 'commonConfig', '$timeout', '$interval'];

export default function configService($rootScope, $q, $window, commonConfig, $timeout, $interval) {
    
    var service = {
        $broadcast: $broadcast,
        $rootScope: $rootScope,
        $q: $q,
        $window: $window,
        broadcastErrorDialogEvent: broadcastErrorDialogEvent,
        activateParentController: activateParentController,
        activateController: activateController
    }

    function $broadcast() {
        return $rootScope.$broadcast.apply($rootScope, arguments);
    }

    function broadcastErrorDialogEvent(error) {
        $broadcast(commonConfig.config.showErrorDialogEvent, error);
    }
    
    function activateParentController(requests, controllerId) {
        return $q.all(requests).then(function (result) {
            var data = {controllerId: controllerId};
            $broadcast(commonConfig.config.activateParentControllerEvent, data);
        });
    }

    function activateController(requests, controllerId) {
        return $q.all(requests).then(function (eventArgs) {
            var data = {controllerId: controllerId};
            $broadcast(commonConfig.config.activateControllerEvent, data);
        }).catch(function (reason) {
            $broadcast(commonConfig.config.activateControllerFailedEvent, reason);
        });
    }

    return service;
}