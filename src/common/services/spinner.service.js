spinnerService.$inject = ['common', 'commonConfig']

export default function spinnerService(common, commonConfig) {
    this.spinnerHide = function() {
        spinnerToggle(false);
    }
    this.spinnerShow = function() {
        spinnerToggle(true);
    }
    function spinnerToggle(show) {
        common.$broadcast(commonConfig.config.spinnerToggleEvent, {show: show});
    }
}