import common from './common.config.service';
import spinner from './spinner.service';

angular.module('common')
    .factory('common', common)
    .service('spinner', spinner)