httpInterceptor.$inject = ['$httpProvider'];

export default function httpInterceptor($httpProvider) {
    var requestInterceptor = ['$q', '$injector', '$location', '$rootScope', '$timeout', '$log',
        ($q, $injector, $location, $rootScope, $timeout, $log) => {
            var timer;
            var numOfRequests = 0;
            var hide = () => {
                numOfRequests--;
                if (numOfRequests <= 0) {
                    $rootScope.$emit('spinner.toggle', {show: false});
                }
            };
            var show = () => {
                numOfRequests++;
                if (numOfRequests > 0) {
                    $rootScope.$emit('spinner.toggle', {show: true, background: false});
                }
            };
            var interceptorInstance = {
                request: function (config) {
                    show();
                    return config || $q.when(config);
                },
                response: function (response) {
                    hide();
                    return response || $q.when(response);
                },
                responseError: function (rejection) {
                    hide();
                    if (rejection.config && rejection.config.skipProcessing) {
                        // dont treat as normal data, this is for cors calls
                        if (rejection.status === 0) {
                            return $q.reject(rejection);
                        }
                    }
                    else {
                        if (rejection.status === 0) {
                            // network issue?
                            var a = confirm('Not online (re-connect), retry?');
                            if (a) {
                                var defer = $q.defer();
                                $timeout(() => {
                                    var $http = $injector.get('$http');
                                    defer.resolve($http(rejection.config));
                                }, 250);
                                return defer.promise;
                            }

                        }
                        else if (rejection.status === 401) {
                            return;
                        }
                    }
                    if (rejection.status && (rejection.status === 500 || rejection.status === 504)) {
                        return $q.reject(rejection);
                    }
                    if (rejection.status && (rejection.status > 399) && (rejection.status < 500)) {
                        return $q.reject(rejection);
                    }
                    if (rejection.status && (rejection.status === 404 || rejection.status === 405)) {
                        return $q.reject(rejection);
                    }

                    return rejection;
                }

            };
            return interceptorInstance;
        }];

    $httpProvider.interceptors.push(requestInterceptor);
}