import angular from 'angular';
import routing from './app.router';
import decorators from './app.decorators';
import httpInterceptor from './app.interceptors';
import values from './app.values';
import events from './app.events.config';
import commonModule from './common/common.module';
import dashboardModule from './dashboard/dashboard.module';

angular.module('sep', [
		//3rd-party
		'ui.router',
		'ui.bootstrap',
		
		//Applications
		'common',
		'dashboard'
	])
	.value('config', values)
	.config(routing)
	.config(decorators)
	.config(httpInterceptor)
	.config(events)
	.config(['$httpProvider', '$compileProvider', ($httpProvider, $compileProvider) => {
		//todo: Handle request headers etc..
		//this is a performance change to 1.3 will stop adding ng-binding class to each scope
		//Donno what it means
        $compileProvider.debugInfoEnabled(false);
	}])

require('./home/services');
require('./home/components');
require('./home/controllers');
