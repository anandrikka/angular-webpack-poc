import config from './app.values';

commonConfig.$inject = ['commonConfigProvider'];

export default function commonConfig(commonConfigProvider) {
    commonConfigProvider.config.spinnerToggleEvent = config.events.spinnerToggle;
    commonConfigProvider.config.showErrorDialogEvent = config.events.showErrorDialog;
    commonConfigProvider.config.activateParentControllerEvent = config.events.activateParentController;
    commonConfigProvider.config.activateControllerEvent = config.events.activateController;
    commonConfigProvider.config.activateControllerFailedEvent = config.events.activateControllerFailed;
}